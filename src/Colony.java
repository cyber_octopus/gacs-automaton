import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.BitSet;
import java.util.Iterator;
import java.util.Vector;

import com.sun.org.apache.bcel.internal.classfile.Constant;
import com.sun.xml.internal.bind.v2.runtime.reflect.opt.Const;

/**
 * A 1D colony in Gray's model.
 * @author lana
 *
 */
public class Colony {
	/** log*/
	MyLog mlog = new MyLog("colony", true);
	
	/** colony size Q*/
	int q;
	/** cells inside the colony*/
	Cell[] cells;
	/** virtual cell simulated by the colony*/
	VirtualCell vcell;
	
	/**
	 * builds a colony of size Constants.Q
	 * @param s the L1 state (not used yet)
	 */
	public Colony(BitSet s){
		this.q = Constants.Q;
		cells = new Cell[q];
		
		//initalize the cells, their addresses and neighbors
		//store neighborhoods for initalization efficency
		Vector<Cell> ln = new Vector<Cell>();
		for(int i=0;i<q;i++){
			//st = the initial value of the primary simbit
			boolean st = false;
			if(i>=Constants.super_c_index && i <Constants.state_nbits){
				st = s.get(i-Constants.super_c_index);
			}
			Cell c = new Cell(i,st);
			
			//limit conditions left neighborhood
			int ls = ln.size();
			c.setLeftRange(ls);
			//set our neighbors
			for(int j=0; j<ls;j++){
				Cell c2 = ln.get(ls-j-1);
				c.setNeighbor(-(j+1), c2);
			}
			
			cells[i] = c;
			
			//update live neighborhood
			ln.addElement(c);
			if(ln.size()>Constants.range){
				ln.removeElementAt(0);
			}//*/
		}
		
		//do a second pass for initializing the right neighborhood
		///*
		Vector<Cell> rn = new Vector<Cell>();
		//right to left sweep
		for(int i = q-1; i>=0; i--){
			Cell c = cells[i];
			
			int rs = rn.size();
			c.setRightRange(rs);
			//set our neighbors
			for(int j=0; j<rs;j++){
				Cell c2 = rn.get(j);
				c.setNeighbor(j+1, c2);
			}
			
			//update live neighborhood
			//[1,..,range]
			rn.add(0, c);
			if(rn.size()>Constants.range){
				rn.removeElementAt(Constants.range);
			}
		}		
		
		//init virtual cell
		//vcell = new VirtualCell(this); //dummy address
	}
	

	/**
	 * sets the neighborhood of the cells on the left frontier of this colony
	 * @param c the colony to the left of this colony
	 */
	public void setLeftColony(Colony c){
		for(int i=0;i<Constants.range;i++){
			Cell cell = cells[i];
			cell.setLeftRange(Constants.range);
			//our neighbors in our colony
			//TODO why we doing that here??? should be earlier
			int k = 0;
			for(int j=i;j>0;j--){
				k--;
				Cell cell2 = cells[j-1];
				cell.setNeighbor(k, cell2);
			}
			//our neighbors in left colony
			int n = Constants.range+k;//number neighbors to be initialized
			for(int j=0; j<n;j++){
				//index in that colony
				int i2 = q-j-1;
				Cell cell2 = c.getCell(i2);
				cell.setNeighbor(k-j-1, cell2);
			}
		}
	}
	
	
	/**
	 * set the neighborhood of the cells on the right frontier of this colony
	 * @param c the colony to the right of this colony
	 */
	public void setRightColony(Colony c){
		for(int i=q-Constants.range;i<q;i++){
			Cell cell = cells[i];
			cell.setRightRange(Constants.range);
			//our neighbors in our colony
			int k = 0;//neighbor index
			for(int j=i+1;j<q;j++){
				k++;
				Cell cell2 = cells[j];
				cell.setNeighbor(k, cell2);
			}
			//our neighbors in right colony
			int n = Constants.range-k;//number of neighbors to be initialized
			for(int j=0; j<n;j++){
				//mlog.say("j "+j);
				Cell cell2 = c.getCell(j);
				cell.setNeighbor(k+1+j, cell2);
			}
		}
	}

	/**
	 * 
	 * @param i the cell index (address)
	 * @return the cell at this index in the colony
	 */
	private Cell getCell(int i) {
		Cell c = cells[i];
		return c;
	}
	

	/**
	 * apply transfer rules to each cell in the colony,
	 * but do not change current state yet -- that must be synchronized.
	 * @param e error rate
	 */
	public void compute(double e) {
		//calculate new states, but don't update it yet -- wait until everyone has finished.
		for(int i=0;i<q;i++){
			Cell c = cells[i];
			c.fastCompute(e);
		}
	}

	/**
	 * apply transfer rules to each cell in the colony,
	 * with default error rate e.
	 * but do not change current state yet -- that must be synchronized.
	 */
	public void compute() {
		compute(Constants.e);
		//calculate new states, but don't update it yet -- wait until everyone has finished.
	}
	
	/**
	 * update current state to previously computed state.
	 */
	public void update(){
		//synchronized update of states
		for(int i=0;i<q;i++){
			Cell c = cells[i];
			c.update();
			/*if(i==5){
				mlog.say("cell 5: "+c.toString());
			}*/
		}
	}
	
	
	/**
	 * graphics
	 * @param g2d graphics
	 * @param cindex colony index
	 * @param n number of cells of the colony to draw (colony is larger in reality)
	 */
	public void draw(Graphics2D g2d, int cindex, int n) {
		int start = 50;//px
		int c_size = 20;//cell size
		int offset = 320;//30;// (offset between two colonies) TODO DIRTY
		
		//draw limit
		g2d.setColor(Color.black);
		g2d.drawLine(start+offset*cindex, start-25, start+offset*cindex, start+c_size+5);
		
		//draw cells
		int c = Math.min(n, cells.length);
		for(int i=0;i<c;i++){
			cells[i].draw(i,start+offset*cindex,start, cindex ,c_size,g2d);
		}
		//if truncated
		if(c<cells.length){
			//draw indication that graphics are truncated
			g2d.setColor(Color.black);
			int x = start+c*c_size;
			int y = start+cindex*c_size+offset*cindex+(2*c_size/3);
			String s = "  [...]";
			g2d.drawString(s, x, y);
		}
	}


	public int getAddress() {
		int a = 0;
		
		//say we have no offset
		int start = Constants.fields_bit_i()[Constants.address_i];
		int end = Constants.a_nbits;
		BitSet bitset = new BitSet(Constants.a_nbits);
		for (int i = start; i < end; i++) {
			if(cells[i].getSimBit()){
				bitset.set(i);
			}
		}
		
		a = Constants.bitsetToInt(bitset);
		
		
		return a;
	}

	/**
	 * @return state of the L1 cell represented by this colony
	 */
	public String leveStateToString() {
		String l1state = "";
		for(int i=0; i<Constants.Q; i++){
			if(cells[i].getSimBit()){
				l1state+= "1";
			}else{
				l1state+= "0";
			}
			if(i<(Constants.Q-1)){
				l1state+= ",";
			}
		}
		l1state+= "\n";
		return l1state;
	}
	
	/**
	 * @return contents of the xth bit of the workspaces of its cells
	 */
	public String wsToString(int bitindex) {
		//int start = Constants.fields_bit_i()[Constants.workspace_i] + Constants.workspace_info_s + bitindex;
		
		String l1state = "";
		for(int i=0; i<Constants.Q; i++){
			BitSet b = cells[i].getState().getSubField(Constants.workspace_i, Constants.workspace_info_s + bitindex, 1);
			if(b.get(0)){
				l1state+= "1";
			}else{
				l1state+= "0";
			}
			if(i<(Constants.Q-1)){
				l1state+= ",";
			}
		}
		l1state+= "\n";
		return l1state;
	}
}
