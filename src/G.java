import java.util.BitSet;


/**
 * The Gacs Programing language
 * Gacs p65
 * 
 * rules are represented by bitstrings.
 * rules do not change for an automaton.
 * parameters of rules can change.
 * rules are invoked in each cell in the same wat at each timestep.
 * 
 *  Set of rules can be stored in parameters.
 *  Parameters need to be interpreted using a bitstring interpr.
 * 
 * Retrieve: Retrieve the states of the represented neighbor cells from the neighbor colonies;
 * Evaluate: Compute the new state using Tr 2;
 * Update: Replace the old represented state with the new one.
 * 
 * @author lana
 *
 */
public class G {
	/** log*/
	public static MyLog mlog = new MyLog("G", true);
	/** set false for testing */
	public static boolean autonomous = Constants.autonomous;

	/** number of colonies in neighborhood + self */
	static int max_rounds = 1 + Constants.range;
	/**
	 * contains all the rules of the automaton
	 */
	BitSet param_0;
	/** field indices */
	static int[] fields_bit_i = Constants.fields_bit_i();
	/** sizes */
	static int[] field_sizes = Constants.field_sizes;
	

	/**
	 * moves value of a colony's track to the workspace subfield of a range of cells "i" colonies further
	 * to_field: workspace ; to subfield: ws bit index
	 * all fields are level 1 fields (colony level)
	 * This operation takes max (5+1)Q time steps. (or f(i) ?)
	 * writes in cell's next state.
	 * p67
	 * @param cell the cell considered
	 * @param age the result of age%cycle_length
	 * @param i neighbor colony index; use 0.1 or -0.1 for intra-colony mailing; + = to the left, - = to the right
	 * @param from_field COLONY field to copy from (only use canonical fields index)
	 * @param from_subfield 0 or inner bit index of a subfied
	 * @param to_field CELL field to copy to (only use canonical fields index)
	 * @param to_subfield 0 or inner bit index of a subfied
	 * @param from_start where to start copying from source colony
	 * @param to_start where to start writing in this colony
	 * @param length of the bitstring to copy
	 * @param time_start time to start the operation
	 * @param return true if the operation is finished
	 */
	/*public static boolean copy(Cell cell, int age, int i, int from_field, int from_subfield, int to_field, int to_subfield,
			int from_start, int to_start, int length, int time_start) {
		
		int from_bit = fields_bit_i[from_field]+from_subfield;
		int to_bit = fields_bit_i[to_field]+to_subfield;
		return copyGeneral(cell, age, i, from_bit, to_bit, from_start, to_start, length, time_start);	
	}*/
	//TODO remove above?
	
	
	/**
	 * moves value of a colony's track to the workspace subfield of a range of cells "i" colonies further
	 * to_field: workspace ; to subfield: ws bit index
	 * all fields are level 1 fields (colony level)
	 * This operation takes max (5+1)Q time steps. (or f(i) ?)
	 * writes in cell's next state.
	 * p67
	 * @param cell the cell considered
	 * @param age the result of age%cycle_length
	 * @param i neighbor colony index; use 0.1 or -0.1 for intra-colony mailing; + = to the left, - = to the right
	 * @param from_field COLONY field to copy from (only use canonical fields index)
	 * @param from_subfield 0 or inner bit index of a subfied
	 * @param to_subfield inner bit index of workspace
	 * @param from_start where to start copying from source colony
	 * @param to_start where to start writing in this colony
	 * @param length of the bitstring to copy
	 * @param time_start time to start the operation
	 * @param return true if the operation is finished
	 */
	public static boolean copyToWS(Cell cell, int age, int i, int from_field, int from_subfield, int to_subfield,
			int from_start, int to_start, int length, int time_start) {
		
		int from_bit = fields_bit_i[from_field]+from_subfield;
		return copyToWSGeneral(cell, age, i, from_bit, to_subfield, from_start, to_start, length, time_start);	
	}
	
	
	/**
	 * Copies an interval of a colony's state to a track in a colony i colonies further.
	 * all fields are level 1 fields (colony level)
	 * This operation takes (5+1)Q time steps (max)
	 * writes in cell's next state.
	 * p67
	 * @param cell the cell considered
	 * @param age the result of age%cycle_length
	 * @param i neighbor colony index; use 0.1 or -0.1 for intra-colony mailing; + = to the left, - = to the right
	 * @param from_bit index of the bit !!in colony state!!! to start copying from
	 * @param to_wsi index of the bit in the workspace !!in the cell!! to write the retrieved info
	 * @param from_start where to start copying from source colony and to start writing in this colony
	 * @param to_start where to start writing in this colony
	 * @param length of the bitstring to copy
	 * @param time_start time to start the operation
	 * @param return true if the operation is finished
	 */
	public static boolean copyToWSGeneral(Cell cell, int age, int i, int from_bit, int to_wsi,int from_start,
										int to_start, int length, int time_start) {

		//copy must be parallel: write result in next_state.		
		CellState old_state = cell.state;
		CellState new_state = cell.nextState;
		//check if message is for us
		int ad = cell.getAddress();
		//write sender id and message. this is done once, let's say it is done at age=0
		if(age==time_start){
			BitSet mail = new BitSet(Constants.mailbox_nbits/5);
			//set sender address
			mail.or(old_state.getAddressBits());
			//new_state.setMailFrom(old_state.getAddressBits(), 0);//address of sending cell

			//check if info is in this cell
			int info_end = from_bit+length;
			if(ad>=from_bit && ad<info_end){
				//write info from the simbit ( == state bit of index "from_bit" in L1)
				if(old_state.getField(Constants.simBit_i).get(0)){
					mail.set(Constants.a_nbits);
				}
			}
			
			new_state.setMail(mail, 0);
			cell.setBackupsMails(mail);
			
			return false;
		}
		
		// /!\ a modification by me
		//get the direction of message passing
		int abs_i = Math.abs(i);
		int sign_i = abs_i/i;
		
		//this is done for some time using age field
		//time is 2Q in Gacs: max time necessary to move 1 bit from end of C2 to beginning of C1
		//In Gray: (5+1)Q
		if(age>time_start && age<time_start+(6*Constants.Q)){
			//move mail
			move_mail(cell,sign_i);
			//sender address
			int fromAd = Constants.bitsetToInt(old_state.getMailFrom(0));
			
			//stage in the current copy operation
			int coop = age%(max_rounds*Constants.Q);
			//how many steps until this cell starts reading from a different colony
			int stepleft;
			if(i>0){
				stepleft = Constants.Q - ad;
			}else {
				stepleft = ad+1;
			}
			//colony we are receiving from now
			int recol = (coop - stepleft + Constants.Q)/ Constants.Q;
			//check that we are in the right space interval NO
			//write only if bit index in the received track is == to this cell's address
			int start_sender = from_bit;
			if((fromAd-start_sender == ad+to_start)
					&& (recol == abs_i)){
				boolean b = old_state.getMailInfo(0).get(0);
				//write into this cell
				new_state.setWSi(to_wsi, b);
				//write backups
				cell.setBackupWSi(to_wsi, b);
			}
			return false;
		}//else copy is finished
		
		return true;		
	}
	
	
	/**
	 * Copies an interval of a colony's state to a track in a colony i colonies further.
	 * all fields are level 1 fields (colony level)
	 * This operation takes (5+1)Q time steps (max)
	 * writes in cell's next state.
	 * p67
	 * @param cell the cell considered
	 * @param age the result of age%cycle_length
	 * @param i neighbor colony index; use 0.1 or -0.1 for intra-colony mailing; + = to the left, - = to the right
	 * @param from_bit index of the bit !!in colony state!!! to start copying from
	 * @param to_bit index of the bit !!in the cell!! to write the retrieved info
	 * @param from_start where to start copying from source colony and to start writing in this colony
	 * @param to_start where to start writing in this colony
	 * @param length of the bitstring to copy
	 * @param time_start time to start the operation
	 * @param return true if the operation is finished
	 */
	/*public static boolean copyGeneral(Cell cell, int age, int i, int from_bit, int to_bit,int from_start,
										int to_start, int length, int time_start) {

		//copy must be parallel: write result in next_state.		
		CellState old_state = cell.state;
		CellState new_state = cell.nextState;
		//check if message is for us
		int ad = cell.getAddress();
		//write sender id and message. this is done once, let's say it is done at age=0
		if(age==time_start){
			BitSet mail = new BitSet(Constants.mailbox_nbits/5);
			//set sender address
			mail.or(old_state.getAddressBits());
			//new_state.setMailFrom(old_state.getAddressBits(), 0);//address of sending cell

			//check if info is in this cell
			int info_end = from_bit+length;
			//mlog.say("info end "+info_end);
			if(ad>=from_bit && ad<info_end){
				//write info from the simbit ( == state bit of index "from_bit" in L1)
				if(old_state.getField(Constants.simBit_i).get(0)){
					mail.set(Constants.a_nbits);
				}
				//new_state.setMailInfo(old_state.getField(Constants.simBit_i), 0);
			}
			
			new_state.setMail(mail, 0);
			cell.setBackupsMails(mail);
			
			return false;
		}
		
		// /!\ a modification by me
		//get the direction of message passing
		int abs_i = Math.abs(i);
		int sign_i = abs_i/i;
		
		//this is done for some time using age field
		//time is 2Q in Gacs: max time necessary to move 1 bit from end of C2 to beginning of C1
		//In Gray: (5+1)Q
		if(age>time_start && age<time_start+(6*Constants.Q)){
			//move mail
			move_mail(cell,sign_i);
			//sender address
			int fromAd = Constants.bitsetToInt(old_state.getMailFrom(0));
			
			//stage in the current copy operation
			int coop = age%(max_rounds*Constants.Q);
			//how many steps until this cell starts reading from a different colony
			int stepleft;
			if(i>0){
				stepleft = Constants.Q - ad;
			}else {
				stepleft = ad+1;
			}
			//colony we are receiving from now
			int recol = (coop - stepleft + Constants.Q)/ Constants.Q;
			//check that we are in the right space interval NO
			//write only if bit index in the received track is == to this cell's address
			int start_sender = from_bit;
			if((fromAd-start_sender == ad+to_start)
					&& (recol == abs_i)){
				//write into this cell
				//mlog.say("wrote info "+ old_state.getMailInfo().get(0) + " at cell " + ad) ;//" bit "+ to_bit);
				new_state.setBit(to_bit, old_state.getMailInfo(0).get(0));
			}
			return false;
		}//else copy is finished
		
		return true;		
	}*/
	//TODO remove above?
	
	/**
	 * retrieve addresses from the 5 right neighboring colonies and update the simbit (part of simulated address) of the cell that called
	 * TODO we should not start storage at cell 1
	 * p
	 * @param cell
	 */
	public static void correct_address(Cell cell){
		if(!autonomous){
			return;
		}
		int age = cell.getAge();
		//useless age = age%Constants.L1_cycle;
		int addr = cell.getAddress();
		
		/*if(age<(6*Constants.Q)){
			//copy full address of i neighbor colony into workspace 1s't bit
			G.copy(cell,1,Constants.address_i,0,Constants.workspace_i,
					Constants.workspace_info_s,0,0,Constants.a_nbits,0);
			//return to prevent checking condition uselessly
			return;
		}//*/
		
		//from 0*6*Q to 5*6*Q[ 8790
		//copy from 5 right neighbors
		for (int i = 0; i <5; i++) {
			//check if time is right to start copying
			if(age<(i+1)*(6*Constants.Q)){
				//copy full state of i neighbor colony into each cell's workspace in this colony
				G.copyToWS(cell, age, i+1, Constants.address_i,0,i,0,0,Constants.state_nbits,i*(6*Constants.Q));
				//return to prevent checking condition uselessly
				return;
			}
		}//*/
		
		//clean up mail fields
		cell.nextState.clearMailbox(0);
		cell.clearBackupsMails();
		
		//from 5*6*Q to 10*6*Q[ 17580
		//copy from 5 left neighbors
		for (int i = 0; i >-5; i--) {
			//check if time is right to start copying
			if(age<(-i+1+5)*(6*Constants.Q)){//age<(-i+1+5)
				//copy full address of i neighbor colony  into our workspace
				G.copyToWS(cell, age, i-1, Constants.address_i,0,(-i+5),0,0,Constants.a_nbits,(-i+5)*(6*Constants.Q));
				//return to prevent doing many operations on same count
				return;
			}
		}//*/
		
		//clean up mail fields
		cell.nextState.clearMailbox(0);
		cell.clearBackupsMails();

		//adjust colony addresses and age	
		//from previous to (see below)
		int start = 10*6*Constants.Q;
		//duration of one -1 operation
		int duration = Constants.Q*2;
		//this code is left for comprehension
		//shift addresses: -1..-5 (from right neighbors
		//this state.ws_i -=1; if necessary trickle to higher bits using mail
		/*if(age<start+duration){
			wsi_minus_one(cell, 0, start);// once for neighbor 1
			return;
		}
		start+=duration;
		for(int i=0; i<2; i++){
			if(age<start+duration*(i+1)){
				wsi_minus_one(cell, 1, start+i*duration);// 2x for neighbor 2
				return;
			}
		}*/
				
		//generalisation of above code
		//if in address field of L1, do -1 to -5
		int adr_start_info = fields_bit_i[Constants.address_i]; 
		int adr_end_info = adr_start_info + Constants.a_nbits;
		//age field
		int age_start_info = fields_bit_i[Constants.age_i]; 
		int age_end_info = age_start_info + Constants.a_nbits;
		if(addr>= adr_start_info && addr<adr_end_info){
			//starts from 10*6*Q
			for(int j=0; j<5; j++){
				start+=duration*(j);
				for(int i=0; i<(j+1); i++){//-3 for j=2, etc
					if(age<start+duration*(i+1)){
						wsi_minus_one(cell, age, j, start+i*duration, adr_start_info, adr_end_info);
						return;
					}
				}
			}	
		}else if (addr>= age_start_info && addr<age_end_info) {
			//in parallel with above
			//if in age field of L1, add 1 to each ws_i
			for(int j=0; j<5; j++){//why no start+=.. as in previous loop?
				if(age<start+duration*(j+1)){
					wsi_plus_one(cell, age, j, start+j*duration, age_start_info, age_end_info);
					return;
				}
			}	
		}
		
		//wait for operations on address to finish
		//longest operation (-5 for address) finishes at (10*6)*Q + (Q*2)*(1+2+...+4)
		// 60*Q + Q*2*10 = (60 + 20)Q = 23 440 
		if(age < 23440){
			return;
		}

		//TODO if any of the numbers is negative, add Q (actually, add number of colonies,
		//which will need to be Q at some point) (implement +1 function)
		//even better: if any of the numbers looks like it's gonna be negative,set it to Q then do -1
		
		//take the majority in received bits (just right side for now)
		//age
		//right = ws 0..4
		//from t= 23 440 to +2*Q: 24 026 (or just to x+1??)
		start = start+ duration*(1+2+3+4);
		duration = 1;

		//if simbit in age field of L1, take majority
		int start_info = fields_bit_i[Constants.age_i]; 
		int end_info = start_info + Constants.a_nbits;
		if(addr>= start_info && addr<end_info){// 
			//take majority
			if(age<start+duration){
				boolean value = false;
				//neighbors info in workspace
				BitSet b = cell.nextState.getSubField(Constants.workspace_i, Constants.workspace_info_s, 5);
				int set = b.cardinality();
				if(set>2){//majority is true
					value = true;
				}			
				cell.nextState.setSimBit(value);	
				//set it for the neighbors too
				cell.setBackupSimbits(value);
				return;
			}
		}
	
		//wait...
		if(age<Constants.L1_cycle-1){
			return;
		}
		
		//clean workspace up here	
		cell.nextState.clearWorkspaceInfo(0);
		cell.clearBackupWS();
		//clear mails
		cell.nextState.clearMailbox(0);
		cell.clearBackupsMails();
		
		//TODO here compute consistency before we compute anything else
		
		return;
	}
	
	/**
	 * perform bitwise operations on a workspace track to do an addition
	 * @param cell
	 * @param age the result of age%cycle_length
	 * @param wsi ws bit index (do no count flags)
	 * @param age_start
	 * @param lim_r right limit (index of last cell on the right)
	 * @param lim_l left limit
	 */
	public static void wsi_plus_one(Cell cell, int age, int wsi, int age_start, int lim_l, int lim_r){
		int field_i = Constants.workspace_i;
		int address = cell.getAddress();

		//cell at the exact right address
		//takes max size(int)*2 timesteps to do +1 and trickle
		int stage = age-age_start;
		//if odd number, move mail to right neighbor
		if(stage%2==1){
			if(address<lim_r & address>lim_l){
				move_mail(cell, -1);
			}
		}else{
			//get mail
			BitSet mail = cell.state.getMailInfo(0);
			
			//clean mailbox
			cell.nextState.clearMailbox(0);
			cell.clearBackupsMails();

			//if even number, divide by 2 and perform calculation
			stage = stage/2;
			if((stage+lim_l) == address){
				BitSet b = cell.state.getSubField(field_i, Constants.workspace_info_s+wsi, 1);	
				if(address==lim_l || mail.get(0)){
					//flip
					b.flip(0);
					cell.nextState.setSubField(field_i,  Constants.workspace_info_s+wsi, 1, b);
					cell.setBackupWSi(wsi, b.get(0));
					
					//if b+1 gave 0 and not 1, we must add 1 to next cell
					if((!b.get(0)) && address<lim_r){
						mail = new BitSet(Constants.a_nbits+1);
						mail.or(cell.state.getAddressBits());//set sender
						mail.set(Constants.a_nbits);//set info
						cell.nextState.setWoleMail(mail, 0);
						cell.setBackupsMails(mail);
					}
				}
				//last bit = signed bit; bit before last = biggest bit
			}
		}
	}	
	
	//TODO make it look more like previous function
	/**
	 * perform bitwise operations on a workspace track to do a substraction
	 * @param cell
	 * @param age the result of age%cycle_length
	 * @param wsi ws bit index (do not count flags)
	 * @param age_start
	 * @param lim_r right limit for passing mails (index of last cell on the right)
	 * @param lim_l left limit for passing mails
	 */
	public static void wsi_minus_one(Cell cell, int age, int wsi, int age_start, int lim_l, int lim_r){
		int field_i = Constants.workspace_i;
		int address = cell.getAddress();

		//cell at the exact right address
		//takes max size(int)*2 timesteps to do -x and trickle
		int stage = age-age_start;
		//if odd number, move mail to right neighbor
		if(stage%2==1){
			//only if neighbor is not rightmost
			if(address<lim_r){
				move_mail(cell, -1);
			}
		}else{
			BitSet mail = cell.state.getMailInfo(0);
			//clean mailbox 
			cell.nextState.clearMailbox(0);
			cell.clearBackupsMails();

			//if even number, divide by 2 and perform calculation
			stage = stage/2;
			if((stage+lim_l) == address){
				BitSet b = cell.state.getSubField(field_i, Constants.workspace_info_s+wsi, 1);	

				if(address==lim_l || mail.get(0)){
					//flipped
					b.flip(0);
					cell.nextState.setSubField(field_i, Constants.workspace_info_s+wsi, 1, b);
					cell.setBackupWSi(wsi, b.get(0));
					
					//BitSet maj = cell.majority(Constants.workspace_i, Constants.workspace_info_nbits, 2);
					//mlog.say("value should be " + b.get(0) + " and is "+ maj.get(wsi));
					
					//and pass
					if(b.get(0) && address<lim_r){
						//if b-1 gave 1 and not 0, we must remove 1 from next cell
						mail = new BitSet(Constants.a_nbits+1);
						mail.or(cell.state.getAddressBits());//set sender
						mail.set(Constants.a_nbits);//set info
						cell.nextState.setWoleMail(mail, 0);
						cell.setBackupsMails(mail);
					}
					
				}
				//last bit = signed bit; bit before last = biggest bit
			}
		}
	}	
	
	/**
	 * copies right neighbor's mail into our next_state.mail
	 * also set mail backups
	 * @param cell this cell
	 * @param dir direction of mail motion (+1 = to the left, -1 = to the right
	 */
	public static void move_mail(Cell cell, int dir) {
		Cell n = cell.getNeighbor(dir);		
		CellState state = cell.nextState;
		
		BitSet mail = n.state.getWholeMail(0);
		state.setWoleMail(mail, 0);	
		cell.setBackupsMails(mail);
	}
}
