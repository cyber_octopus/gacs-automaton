/**
 * This class initializes and launches the automaton.
 * @author lana
 *
 */
public class Starter {

	public static void main(String[] args) {	
		/** logger*/
		MyLog mlog = new MyLog("GrayStarter",true);

		mlog.say("Q: "+Constants.Q);
		mlog.say("U: "+Constants.U);
		mlog.say("bits for Q*U: "+ Constants.qu_bits);

		mlog.say("workspace bits: "+ Constants.workspace_nbits);
		mlog.say("total size of state: "+ Constants.state_nbits);
		mlog.say("simbit position: "+ Constants.fields_bit_i()[Constants.simBit_i]);

		Constants.fields_bit_i();
		int start = Constants.fields_bit_i()[Constants.workspace_i] + Constants.workspace_info_s;
		mlog.say("this "+ start);
		
		try {
			Thread.sleep(0000);//for profiling
		} catch (InterruptedException e) {
			e.printStackTrace();
		}		
		
		new Automaton();
	}	
}
