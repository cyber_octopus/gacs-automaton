import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.Vector;

import javax.swing.text.DefaultEditorKit.CopyAction;

import com.sun.xml.internal.bind.v2.runtime.reflect.opt.Const;

/**
 * A cell in Gray's model.
 * The transistion rules are in this class.
 * @author lana
 *
 */
public class Cell {
	//debug only
	//public MyLog mlog = new MyLog("cell", false);
	
	/** left neighborhood of this cell. We can read data from these cells.
	 * order: from right (0) to left (5)*/
	Cell[] l_neigh = new Cell[0];
	/** right neighborhood of this cell. We can read data from these cells.
	 * order: from left to right*/
	Cell[] r_neigh = new Cell[0];
	/** the state of the cell*/
	CellState state;
	
	/**used to store state at t+1, calculated from neighborhood at t */
	CellState nextState;
	/** our address in our apparent colony*/
	int apparent_address;
	/** computed colony position as defined p20. Calculated once at the
	 * very beginning of each time step. */
	boolean hasApparentColony;

	
	/** color coding for some properties of the state*/
	Color in_color = Constants.c_healthy;
	/** border color for what external observer knows about the state*/
	Color out_color = Constants.c_normal;


	/**
	 * A cell in Grays's model, with all the required fields.
	 * @param address the address of this cell in the colony
	 */
	public Cell(int address){
		state = new CellState(address);
		nextState = state.copy();
	}
	
	/**
	 * A cell belonging to a colony that simulates a super cell.
	 * @param address the address of this cell
	 * @param b the bit destined to be stored in the SimBit field
	 */
	public Cell(int address, boolean b) {
		state = new CellState(address, b);
		nextState = state.copy();
	}

	/**
	 * Beware of limit conditions (cells at the limits of the finite lattice)
	 * @param l left neighborhood range
	 */
	public void setLeftRange(int l){
		l_neigh = new Cell[l];
	}
	
	/**
	 * Beware of limit conditions (cells at the limits of the finite lattice)
	 * @param r right neighborhood range
	 */
	public void setRightRange(int r){
		r_neigh = new Cell[r];
	}
	
	/**
	 * use this function to set c as our neighbor at this index. 
	 * beware of limit conditions (cells with irregular number of neighbors)
	 * @param gi Gray's index. e.g. gi = -1 -- 1st left neighbor
	 * @param c cell to be set as neighbor
	 */
	public void setNeighbor(int gi, Cell c){
		//on the left side
		if(gi<0){
			int index = -gi -1;
			l_neigh[index] = c;
			 //set simbits backup
			if(index<2){
		        int index2 = -(gi-2);
		        c.state.setFlag(Constants.simBit_i,index2,state.primarySimbit());
			}
		}else{
			//on the right side
			int index = gi -1;
			r_neigh[index] = c;
			 //set simbits backup
			if(index<2){
				int index2 = 3-gi;
		        c.state.setFlag(Constants.simBit_i,index2,state.primarySimbit());
			}
		}
	}
	
	
	/**write state to the console*/
	public String toString(){
		String damage = "healthy";
		if(state.flag1()){
			damage = "damaged";
		} 
		String description = damage + "; address "+state.getAddressInt()+ " age "+ state.getAgeInt()+
				" has C: "+ hasApparentColony;
		return description;
	}
	
	
	/**
	 * Calculates next state but don't write it down yet.
	 * more CPU-efficient than compute().
	 */
	public void fastCompute(double e){
		//if no error, compute new state
		if(Constants.uniformDouble()>e){
			out_color = Constants.c_normal;

			
			//build the state at t+1
			nextState = state.copy();
			
			//do the level+ transition
			if(Constants.autonomous){
				G.correct_address(this);	
			}
			
			//apply transfer rules
			globalCompute();		
		}else{
			//next state is an error
			nextState = new CellState(true);
			out_color = Constants.c_damaged;
		}
	}

	
	/**
	 * compressed version of the compute function.
	 * instead of going through each condition separately, do all the counting/calculating work
	 * in one sweep. Keep computation-intensive functions for reference: they're more readable.
	 */
	private void globalCompute(){
		// compute conditions
		//flag 2
		//true if the computed value of Flag1 at x is 0 and no site in L(x)&C(x) has Flag2 equal to 0
		boolean cond_flag2a = true;
		//true if the computed value of Flag1 at x is 1 and no site in L(x) has Flag2 equal to 1
		boolean cond_flag2b = true;
		//true if at least three sqwites in N(x)&C(x) have Workspace.Flag2 equal to 1
		boolean cond_flag2iv = false;
		int count_ncWF2 = 0;
		//true if C(x) does not exist and the computed value of Age at x is divisible by 16
		boolean cond_flag2iii = false;
		//true if the computed value of Flag1 at x is 1 and at least four sites in L(x) have Flag2 equal to 1
		boolean cond_flag2ii = false;
		int count_lF2 = 0;
		//true if at least four sites in (L(x) AND C(x)) have Flag2 equal to 1
		boolean cond_flag2i = false;
		int count_lcF2 = 0;
		
		//flag1
		boolean cond_flag1i = this.inconsistency();
		//true if at least 3 sites in (R(x) and C(x)) have Flag1 equal to 1
		boolean cond_flag1ii = false;
		int count_rcF1=0;
		//true if at least 3 sites in (N(x) and C(x)) have Workspace.Flag1 equal to 1
		boolean cond_flag1iii = false;
		int count_ncWF1=0;
		//true if no more than one site in (R(x) and C(x)) have Flag1 equal to 1.
		boolean cond_flag1a = true;
		
		//workspace flag 1 (p41)
		//true if address in [Q-5, Q-1]
		boolean cond_w_flag1i = false;
		//true if address in [3U/4, 3U/4+2Q)
		boolean cond_w_flag1ii = false;
		//the computed value of SimBit at the site (x-A(x))+(Q-3) is 1 <-- this is definitely unaccessible info
		boolean cond_w_flag1iii = false;
		
		//workspace flag 2 (p41)
		//true if computed value of Address is in [0, 4]
		boolean cond_w_flag2i = false;
		//true if computed value of Age is in  [3U/4, 3U/4+2Q)
		boolean cond_w_flag2ii = false;
		//the computed value of SimBit at the site x&A(x)+3 is 1, where A(x) is the computed value of Address at x
		boolean cond_w_flag2iii = false;// TODO <-- this is definitely unaccessible info
		//the computed value of Flag1 at x is 0
		boolean cond_w_flag2iv = false;
		
		//mailbox
				
		//all left neighbors (direction: from gi = 0 to -left)
		for(int i=0; i<l_neigh.length;i++){
			if(l_neigh[i].flag2()){
				cond_flag2b = false;
				count_lF2++;
				//reduced left neighborhood
				if(i<this.apparent_address){
					cond_flag2a = true;
					count_lcF2++;
				}
			}
			
			//reduced left neighborhood
			if(i<this.apparent_address){
				if(l_neigh[i].workspaceFlag2()){
					count_ncWF2++;
				}
				if(l_neigh[i].workspaceFlag1()){
					count_ncWF1++;
				}
			}
		}
		
		//all right neighbors (direction: from gi = 0 to +right)
		//reduced neighbors count
		int rrn = Constants.Q-this.apparent_address;
		for(int i=0; i<r_neigh.length;i++){
			
			//reduced right neighborhood
			if(i<rrn){
				if(r_neigh[i].workspaceFlag2()){
					count_ncWF2++;
				}
				if(r_neigh[i].workspaceFlag1()){
					count_ncWF1++;
				}
				if(r_neigh[i].flag1()){
					count_rcF1++;
				}
			}
		}
		
		//one-shot conditions batch 1
		if(count_rcF1>=3){
			cond_flag1ii = true;
		}
		if(count_ncWF1>=3){
			cond_flag1iii = true;
		}
		if(count_ncWF2>=3){
			cond_flag2iv = true;
		}
		if(count_lF2>=4 & this.flag1()){
			cond_flag2ii = true;
		}
		if(count_lcF2>=4){
			cond_flag2i = true;
		}
		if(count_rcF1>1){
			cond_flag1a = false;
		}
		
		//apply transition rules
		//one-shot conditions batch 1
		//flag 1
		if((cond_flag1i | cond_flag1ii | cond_flag1iii) & !state.flag1()){
			nextState.setFlag1(true);
			//mlog.say("all conds: "+ cond_flag1i + " " + cond_flag1ii + " " + cond_flag1iii);
		}
		if( (!cond_flag1i) & (!cond_flag1iii) & cond_flag1a & state.flag1()){
			nextState.setFlag1(false);
		} 
		
		//flag1 - dependent
		if(nextState.flag1()){//p33
			//clear the entire mailbox
			for(int i=0; i<5; i++){
				nextState.clearMailbox(i);
			}
			//do not clear the backups
			//this.clearBackupsMails();
			
			if(nextState.address_int!=state.address_int){//p33
				//clear entire sim fields and stored backups in this cell
				nextState.clearSimfields();
			}
		}
		
		//simbit: majority rule on 4 neighbors + self (p33)
	    boolean sim = simbitMajority();
	    nextState.setSimBit(sim);
	    setBackupSimbits(sim);	    
	    //same for mail
	    BitSet maj = majority(Constants.mailbox_i, Constants.mailbox_from_nbits+Constants.mailbox_info_nbits,
	    					  Constants.mailbox_from_s);
	    nextState.setWoleMail(maj, 0);
	    setBackupsMails(maj);    
	    //same for ws
		maj = majority(Constants.workspace_i, Constants.workspace_info_nbits, Constants.workspace_info_s);
		nextState.setSubField(Constants.workspace_i, Constants.workspace_info_s,
							  Constants.workspace_info_nbits, maj);//*/
		for(int i=0; i<11; i++){
			setBackupWSi(i, maj.get(i));
		}
		
		//one-shot conditions batch 2
		if(!nextState.flag1()){
			cond_flag2b = false;
			cond_w_flag2iv = true;
		}else{
			cond_flag2a = false;
		}	
		//!warning! Made-up rule. replaced computed value of age by actual value of age tp avoid circular conditions.
		if(!hasApparentColony & (this.getAge()%16)==0){
			cond_flag2iii = true;
		}
		
		//transition: flag 2
		if((cond_flag2i | cond_flag2ii | cond_flag2iii | cond_flag2iv) & !state.workspaceFlag2()){//when are WF set?
			nextState.setFlag2(true);
		}

		if((!cond_flag2iii & !cond_flag2iv) & (cond_flag2a | cond_flag2b) & state.workspaceFlag2()){
			nextState.setFlag2(false);
		}
	
		//address and age
		//Warning: circular conditions on age and f2!
		//If C(x) exists and either the computed value of Flag1 at x is 0 or the computed value of Flag2 at x is 1,
		//take the majority vote in R(x)
		/** computed [age, address] */
		int[] r;
		if(hasApparentColony & (!nextState.flag1() | nextState.flag2())){
			r = majorities(r_neigh,1);
			nextState.setAgeInt(r[0]);
			nextState.setAddressInt(r[1]);
		}else{
			r = majorities(l_neigh,-1);
			nextState.setAgeInt(r[0]);
			nextState.setAddressInt(r[1]);
		}				
		//increment age
		nextState.ageThisCell();	
		
		
		//one-shot conditions batch 3
		//worspace flag 1
		//address in [Q-5, Q-1]
		if(r[1]>=Constants.Q-5 & r[1]<=Constants.Q-1 & workspaceFlag1()){
			cond_w_flag1i = true;
		}
		//address in [3U/4, 3U/4+2Q). Use decimal numbers?
		int s = 3*Constants.U/4;//start
		int e =s + 2*Constants.Q;//end
		
		if(r[1]>=s & r[1]<e){
			cond_w_flag1ii = true;
		}		
		if(cond_w_flag1i & cond_w_flag1ii){// & cond_w_flag1iii){
			nextState.setWorkspaceFlag1(true);			
		}else{//flag is set & one of the conditions is false (in current implementation iii is always false...)
			nextState.setWorkspaceFlag1(false);
		}
		
		//workspace flag 2
		//address in [0, 4]
		if(r[1]>=0 && r[1]<=4){
			cond_w_flag2i = false;
		}
		//age is in  [3U/4, 3U/4+2Q)
		if((r[0]>= 3*Constants.U/4) && (r[0]<((3*Constants.U/4) + 2*Constants.Q))){
			cond_w_flag2ii = true;
		}
		//the computed value of SimBit at the site x&A(x)+3 is 1, where A(x) is the computed value of Address at x
		//cond_w_flag2iii <-- this is definitely unaccessible info
		if(cond_w_flag2i && cond_w_flag2ii && cond_w_flag2iv){
			nextState.setWorkspaceFlag2(true);
		} else{
			nextState.setWorkspaceFlag2(false);
		}
				
		
		//gave precedence to age calculation over this. therefore, not used.
		if(!hasApparentColony & (nextState.getAgeInt()%16)==0){
			cond_flag2iii = true;
		}
	}

	/**
	 * @return true if flag raised, false otherwise
	 */
	private boolean workspaceFlag1() {		
		return state.workspaceFlag1();
	}


	/**
	 * @return true if flag raised, false otherwise
	 */
	private boolean workspaceFlag2() {		
		return state.workspaceFlag2();
	}

	/**
	 * TODO this way of counting majority seems inefficient.
	 * take majority vote for address and age among the left neighbors.
	 * @param neighbors the neighborhood on which to perform the vote.
	 * @param direction +1 if right neighbors, -1 if left neighbors
	 * @return {age, address}
	 */
	private int[] majorities(Cell[] neighbors, int direction){
		int[] res = {-1,-1};
		int s = neighbors.length;
		//counts how many agree
		int count_ag[] = new int[s];
		int count_ad[] = new int[s];
		//current count
		int c_ag = 0;
		int c_ad = 0;
		
		//calculate adjusted addresses
		int ad[] = new int[s];
		for(int i=0; i<s;i++){
			//the adjusted address
			ad[i] = neighbors[i].getAddress() - (i + 1)*direction;
			// next 2 lines should be : ad[i] = ad[i] % Constants.Q;
			//but java does not have a real modulo operator that works with negative numbers.
			if(ad[i]<0) ad[i]+=Constants.Q;
			if(ad[i]>=Constants.Q) ad[i]-=Constants.Q;
		}
		
		for(int i=0; i<s;i++){
			for(int j=i+1;j<s;j++){
				if(neighbors[i].getAge()==neighbors[j].getAge()){
					count_ag[i]++;
				}
				if(ad[i]==ad[j]){
					count_ad[i]++;
				}
			}	
			if(c_ag<count_ag[i]){
				res[0] = neighbors[i].getAge();
				c_ag = count_ag[i];
			}else if(c_ag==count_ag[i]){//no majority
				res[0] = -1;
			}
			if(c_ad<count_ad[i]){
				res[1] = ad[i];
				c_ad = count_ad[i];
			}else if(c_ad==count_ad[i]){//no majority
				res[1] = -1;
			}
		}
		
		//default values if no majority
		if(res[0]<0){
			res[0] = this.getAge();
		}
		if(res[1]<0){
			res[1] = this.getAddress();
		}
		
		return res;
	}

	/**
	 * Simply update current state to calculated state.
	 */
	public void update() {
		this.state = nextState;
		//graphics
		if(state.flag1()){
			in_color = Constants.c_flagged;
		} else {
			in_color = Constants.c_healthy;
		}
		
		if(nextState.workspaceFlag1()){
			in_color = Color.black;
		}
	}
	
	/**
	 * @return the value of flag1 in the state of this cell
	 */
	public boolean flag1() {
		return state.flag1();
	}
	
	/**
	 * @return the value of flag1 in the state of this cell
	 */
	public boolean flag2() {
		return state.flag2();
	}

	/**
	 * Calculates whether this cells belongs to an apparent colony C or not.
	 * (page 20): at least 3 sites in R have consistent addresses.
	 * @return true if C can be found or if this cell does not have enough neighbors, false otherwise
	 */
	public boolean computeApparentColony(){
		boolean has = false;
		//made up default rule
		apparent_address = this.getAddress();
		
		//there are no neighbors, or not enough neighbors (boundary of the whole lattice):
		//say that apparent colony exists by default.
		if(r_neigh.length<3){
			has = true;
		}else{	
			//do at least 3 sites in R have consistent addresses? 
			//look at the 1st 3 sites on the right (remaining sites in neigh <min number of consistent cells 3)
			for(int i=0; i<(r_neigh.length-2);i++){//-2 to check at most 3 neigh
				//who is i site consistent with, in all R?
				BitSet b = new BitSet(r_neigh.length);
				int adi = r_neigh[i].getAddress();
				//DO NOT use b.size()-> useless method, does not give set size. nor does length.
				for(int j=i+1;j<r_neigh.length;j++){
					//do we have addresses that go in increments of 1?
					int adj = r_neigh[j].getAddress();
					//take other colonies into consideration		
					if(adi>adj){
						adj = adj + Constants.Q;
					}
					int diff = (adj-adi);
					if(diff==(j-i)){
						b.set(j);
					}
				}
				if(b.cardinality()>=2){//number of people who agree with i neighbor'S adresss
					has = true;
					apparent_address = adi-(i+1);
					if(apparent_address<0){
						apparent_address = Constants.Q+apparent_address;
					}
					break;
				}
			}
		}
		
		hasApparentColony = has;
		return has;
	}
	
	/**
	 * there is inconsistency at x if one of the following four conditions holds:
	 * (i) C(x) does not exist, or (ii) C(x) exists, but there are at least three sites in L(x) inter C(x)
	 *  whose Address values are not consistent with the positions of those sites relative to C(x), 
	 *  or (iii) there do not exist three sites in R(x) with the same Age value, 
	 *  or (iv) there exist at least three sites in R(x) with the same Age value a and at least 
	 *  three sites in L(x) inter C(x) whose Age value differs from a.  
	 * 
	 * @return inconsistency as calculated at page 20
	 */
	public boolean inconsistency(){
		
		boolean b = false;
		//condition (i)
		if(!computeApparentColony()){
			b = true;
			//ml.say("no colony");
		}else{
			//condition (ii): C(x) exists,
			//but there are at least three sites in L(x)&C(x) whose Address values 
			//are not consistent with the positions of those sites relative to C(x)
			boolean c2 = false;
			//if number of sites(L&C)>=3 and this app_address>=3 (0,1,2)
			if(apparent_address>=3 & l_neigh.length>=3){
				//calculate 
				//are 3 expected values in LC　not compatible with 3 actual values in LC?
				int count = 0;			
				for(int i=0; i<Math.min(l_neigh.length,apparent_address);i++){
					if(l_neigh[i].getAddress()!=apparent_address-(i+1)){
						count++;
					}
				}
				//if not
				if(count>=3){
					c2 =true;
					//ml.say("no 3 compatible neighbors");
				}
			}
			if(c2){//inconsistency
				b = true;
			}else{
				//condition (iii): there do not exist three sites in R(x) with the same Age value
				boolean c3 = true;// "true" = there DO NOT exist
				
				//not enough neighbors: ignore issues
				if(r_neigh.length<3){
					c3 = false;
					//do nothing
				}else{
					//look for 3 same age
					int r_age = -1;
					for(int i=0; i<r_neigh.length;i++){
						int c_age = r_neigh[i].getAge();
						int count = 0;
						for(int j=0; j<r_neigh.length;j++){
							if(r_neigh[j].getAge()==c_age){
								count++;
							}
						}
						if(count>=3){
							//There DO EXIST
							c3 = false;
							r_age = c_age;
							break;
						}
					}
					if(c3){//inconsistency
						//ml.say("no 3 neighbors with same age");
						b = true;
					}else{
						//condition (iv): there exist at least three sites in R(x) with the same Age value a (= (c3==false))
						// and at least three sites in L(x) & C(x) whose Age value differs from a
						boolean c4 = false;
						if(apparent_address>=3 & l_neigh.length>=3){
							//are 3 values != r_age ?
							int count = 0;			
							for(int i=0; i<Math.min(l_neigh.length,apparent_address);i++){
								if(l_neigh[i].getAge()!=r_age){
									count++;
								}							
							}						
							if(count>=3){
								c4 = true;
							}
						}
						if(c4){//inconsistency
							b = true;
						}
					}		
				}					
			}	
		}
		
		return b;
	}

	public int getAge() {
		return state.getAgeInt();
	}

	/**
	 * @return the integer adress of this cell*/
	public int getAddress() {
		return state.getAddressInt();
	}
	
	/**
	 * 
	 * @return the bitset containing this cell's address
	 */
	public BitSet getAddressBits() {
		return state.getAddressBits();
	}
	
	
	/**
	 * purely for test purposes
	 * @param a adress to be set for this cell.
	 */
	public void setAddress(int a){
		state.setAddressInt(a);
	}
	
	public void setMailFrom(BitSet sender, int copy) {
		state.setMailFrom(sender, copy);
	}


	/**
	 * draws a cell. its color is determined by its state.
	 * @param i index in the colony
	 * @param ox offset
	 * @param oy offset
	 * @param ci colony index
	 * @param c_size size of a cell
	 * @param g2d graphics
	 */
	public void draw(int i, int ox, int oy, int ci, int c_size, Graphics2D g2d) {
		//cell
		g2d.setColor(out_color);
		int x = ox+i*c_size;
		int y = oy;//+ci*c_size;
		//border of this cell
		g2d.draw(new Rectangle2D.Double(x, y, c_size, c_size));
		g2d.setColor(in_color);
		g2d.fillRect(x+1, y+1, c_size-2, c_size-2);
		///*
		String a = ""+this.getAddress();
		g2d.drawString(a, x, y);
		//*/
	}

	public boolean primarySimbit() {
		return state.primarySimbit();
	}

	public Cell getNeighbor(int i) {
		if(i>0){	
			return r_neigh[i-1];
		}
		if(i<0){
			return l_neigh[-i-1];
		}		
		return null;
	}

	public CellState getState() {
		return state;
	}

	/**
	 * 
	 * @return a csv string representing the current state
	 */
	public String stateToString() {
		String s = "";
		//damaged or not
		/*if(out_color == Constants.c_damaged){
			s = s + "TRUE,";
		} else{
			s = s + "FALSE,";
		}*/
		BitSet bits = state.getState();
		for (int i = 0; i < Constants.state_nbits; i++) {
			if(bits.get(i)){
				s = s+"1";
			}else{
				s = s+"0";
			}
			if(i+1<Constants.state_nbits){
				s = s + ",";
			}else{
				s = s+"\n";
			}
		}
		return s;
	}

	public boolean getSimBit() {
		boolean r = state.getField(Constants.simBit_i).get(0);
		return r;
	}

	
	public boolean simbitMajority() {
		BitSet test = majority(Constants.simBit_i, 1, 0);
		return test.get(0);
	}
	
	/**
	 * compares all backup copies
	 * @param from index of first bit
	 * @param size size of info to compare
	 * @param offset offset of info from actual start of field (1-time non backed up flags etc)
	 * @return bitset according to majority
	 */
	BitSet majority(int field_i, int size, int offset) {
		//MyLog mlog = new MyLog("maj", true);
		BitSet result = new BitSet(size);
		int from = Constants.fields_bit_i()[field_i];
		
		//for each bit, take backup majority
		int l = 0;
		for(int k = from; k<(from+size); k++){
			BitSet copies = new BitSet(5);
			//self
			copies.set(0, state.state.get(k+offset));
			//neighbors
			for (int i=0; (i<2 && i<l_neigh.length); i++){
				Cell c = l_neigh[i];
				int copy = -(-i-3);
				int index = size*copy+offset;
				boolean b = c.state.state.get(index+k);
				index = i+1;
				copies.set(index, b);
			}
			for (int i=0; (i<2 && i<r_neigh.length); i++){
				Cell c = r_neigh[i];
				int copy = 2-i;
				int index = size*copy+offset;
				boolean b = c.state.state.get(index+k);
				index = i+3;
				copies.set(index, b);
			}
	
			//majority
			//mlog.say("cardinality " + copies.cardinality());
			boolean r;
		    if(copies.cardinality()>2){
		    	r = true;
		    } else {
		    	r = false;
		    }
		    result.set(l, r);
		    l++;
		}
	    return result;
	}
	
	/**
	 * set backup info into neighbor cells
	 * @param field_index
	 * @param info
	 */
	void setBackups(int field_index, BitSet info){
		int base_size = Constants.field_sizes[field_index]/5;
		int j = 0;
		for(int i=-2; i<3; i++){
			if(i!=0){
				Cell c = getNeighbor(i);
				int index = 4-j;
				int begin = index*base_size;
				c.state.setSubField(field_index, begin, base_size, info);
				c.nextState.setSubField(field_index, begin, base_size, info);
				j++;
			}
		}
	}

	public void setBackupSimbits(boolean bit) {
		BitSet info = new BitSet(1);
		info.set(0, bit);
		setBackups(Constants.simBit_i, info);
	}
	
	void setBackupsMails(BitSet mail){
		setBackups(Constants.mailbox_i, mail);
	}
	
	void clearBackupsMails(){
		BitSet mail = new BitSet(Constants.mailbox_nbits/5);
		setBackupsMails(mail);
	}

	public void setBackupWSi(int to_wsi, boolean b) {
		//MyLog mlog = new MyLog("setBckWsi", true);
		
		int base_size = Constants.workspace_info_nbits;
		int j = 0;
	
		for(int i=-2; i<3; i++){
			if(i!=0){
				Cell c = getNeighbor(i);
				int index = 4-j;
				int begin = index*base_size + to_wsi + Constants.workspace_info_s;
				c.state.setFlag(Constants.workspace_i, begin, b);
				c.nextState.setFlag(Constants.workspace_i, begin, b);
				j++;
			}
		}
	}

	public void clearBackupWS() {
		for(int i=0; i<Constants.workspace_info_nbits; i++){
			setBackupWSi(i, false);
		}
	}

}
