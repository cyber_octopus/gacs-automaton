

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 * Graphic panel
 * @author lana
 *
 */
public class Display extends JFrame {

		private static final long serialVersionUID = 1579747902278268747L;
		//this is awful

		//surface to be drawn on
		Surface s; 
		//window title
		String name = "Gray's Automaton";
		boolean selfRepaint =true;
		
		public Display() {
			//default
			int w = 1000;
			int h = 1000;
			
	        initUI(w,h);
	        this.setVisible(true);
	    }
		
		/**
		 * set custom window size
		 * @param w width
		 * @param h height
		 * @param b should this display self-repaint or not.
		 */
		public Display(int w, int h, boolean b) {
			selfRepaint = b;
	        initUI(w,h);
	        this.setVisible(true);
	    }
		
		public void setName(String name){
			this.name = name;
			setTitle(name);
		}

		/**
		 * 
		 * @param w width
		 * @param h height
		 */
	    private void initUI(int w, int h) {
	        setTitle(name);
	        s = new Surface(w,h);
	        add(s);
	        setSize(w,h);
	        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        setLocationRelativeTo(null);
	        
	        //refresh
	        if(selfRepaint){
		        int delay = 50; //milliseconds
	
		        ActionListener taskPerformer = new ActionListener() {
		          public void actionPerformed(ActionEvent evt) {
		            s.repaint();
		          }
		        };
	
		        new Timer(delay, taskPerformer).start();
	        }else{
	        	  int delay = 50; //milliseconds
	        		
			        ActionListener taskPerformer = new ActionListener() {
			          public void actionPerformed(ActionEvent evt) {
			            s.repaint();
			          }
			        };
		
			        new Timer(delay, taskPerformer).start();
	        }
	    }
	    

	    /**
	     * Add a object to be drawn on the pannel.
	     * @param c the object implementing the component interface
	     */
	    public void addComponent(GraphicalComponent c){
	    	s.addComponent(c);
	    }
	    
	    //hem ?
	    public JPanel getSurface(){
	    	return s;
	    }
	    
	    /**
	     * Add an object to be controlled by keyboard actions.
	     * @param p the object to be controlled
	     * @param string a unique name
	     */
}
	

	/**
	 * The surface on which we draw the graphics.
	 * @author lana
	 *
	 */
	class Surface extends JPanel{
		//default size
		int w;// = 1000;
		int h;// = 1000;

		//list of things to draw
		private List<GraphicalComponent> components = new ArrayList<GraphicalComponent>();
		
		/** build surface with custom size
		 * @param w width
		 * @param h height
		 * */
		public Surface(int w, int h){
			this.w = w;
			this. h = h;
		}
		
		public Surface(){
			w = 1000;
			h = 1000;
		}
		
	    /**
	     * Adds a object to be drawn on the pannel.
	     * @param c the object implementing the component interface
	     */
	    public void addComponent(GraphicalComponent c){
	    	components.add(c);
	    }
	    
		private static final long serialVersionUID = 6523850037367826272L;

		/**
		 * Sets the background grid.
		 * @param g graphics (not used0
		 */
		private void init(Graphics g) {
			this.setOpaque(true);
			this.setBackground(Color.white);
		}

	    @Override
	    public void paintComponent(Graphics g) {

	        super.paintComponent(g);
	        init(g);
	        
	        for(int i=0;i<components.size();i++){
	        	components.get(i).draw(g);
	        }
	    }
	    
	    public int getWidth(){
	    	return w;
	    }
	    
	    public int getLength(){
	    	return h;
	    }
	}
	
	
