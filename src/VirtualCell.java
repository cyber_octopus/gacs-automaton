
public class VirtualCell {
	private CellState state;
	private Colony colony;
	private int address;
	//debug only
	public MyLog mlog = new MyLog("VCell", true);

	public VirtualCell(Colony colony) {
		this.colony = colony;
		//blank state
		state = new CellState(false);
		gatherBits();
	}

	public void gatherBits() {
		Cell[] cells = colony.cells;
		for (int i = Constants.super_c_index; i < Constants.state_nbits; i++) { //just do the address for now// cells.length
			state.setBit(i-Constants.super_c_index, cells[i].primarySimbit());
		}
		address = state.getAddressIntFromBits();
	}

	public int getAddress() {
		return address;
	}

}
