import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.io.FileWriter;
import java.io.IOException;
import java.util.BitSet;

/**
 * Defined as in Gray's paper "Reader's Guide to Gacs' Paper"
 * Contains a lattice and the transition rules for each site in the lattice.
 * @author lana
 *
 */
public class Automaton implements GraphicalComponent {
	/** log*/
	MyLog mlog = new MyLog("Automaton", true);

	/**number of colonies*/
	static int m = Constants.M;
	/** array of colonies existing inside this automaton*/
	static Colony[] colonies = new Colony[m];
	
	
	
	//data saving
	/**data directory*/
	String folderName = "/Users/lana/Development/Gacs_data" ;
	
	public Automaton(){
		//initialize the colonies and their left neighbors
		for(int i=0; i<m; i++){
			//super-state
			CellState state = new CellState(i);
			Colony c = new Colony(state.getState());
			colonies[i] = c;
			if(i>0){
				colonies[i].setLeftColony(colonies[i-1]);
			}
		}
		//cycle
		colonies[0].setLeftColony(colonies[m-1]);
		
		//now that everyone is built do the right neighbors
		for(int i=0; i<m; i++){
			if(i<m-1){
				colonies[i].setRightColony(colonies[i+1]);
			}
			//output the colony address just to check (use pointers here in cpp)
			/*int a = colonies[i].getAddress();
			mlog.say("colony "+ i + " address is "+ a);//*/
		}		
		colonies[m-1].setRightColony(colonies[0]);

		
		/*colonies[0].cells[0].setAddress(1); //TODO weird bug in neighbor colony
		colonies[0].cells[1].setAddress(2);
		colonies[0].cells[2].setAddress(3);
		colonies[0].cells[3].setAddress(4);//*/
		
		/*for(int i=0;i<3; i++){
			int ad = colonies[1].cells[i].getAddress();
			colonies[1].cells[i].setAddress(ad+1);
		}*/
		
		/*BitSet mail = new BitSet(32);
		mail.flip(0, 32);
		colonies[0].cells[31].setMailFrom(mail, 0);
		*/


		if(Constants.draw){
			/**graphics displaying object*/
			Display display;		
			display = new Display();
			display.addComponent(this);
		}
		
		//slow the program down
		try {
			Thread.sleep(0000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		//timestep
		long epoch = 0;
		//follow this
		//VirtualCell vc = colonies[5].vcell;
		//mlog.say("virtual address "+ vc.getAddress());
		
		
		/** records 1 cell's state through time*/
		FileWriter w_c1 = null;
		FileWriter w_c2 = null;
		FileWriter w_c3 = null;
		FileWriter w_c4 = null;
		FileWriter w_c5 = null;
		FileWriter w_c6 = null;
		FileWriter w_c7 = null;
		FileWriter w_c8 = null;
		FileWriter w_c9 = null;
		FileWriter w_c10 = null;

		if(Constants.save){	
			try {
				String weightsFileName = folderName+"/col_0_cell_32e.csv";	
				//TODO the warning
				w_c1 = new FileWriter(weightsFileName);
				mlog.say("stream opened "+weightsFileName);
				
				weightsFileName = folderName+"/col_1_cell_0e.csv";	
				w_c2 = new FileWriter(weightsFileName);
				mlog.say("stream opened "+weightsFileName);
				
				weightsFileName = folderName+"/col_0_cell_33e.csv";	
				w_c3 = new FileWriter(weightsFileName);
				mlog.say("stream opened "+weightsFileName);
				
				weightsFileName = folderName+"/col_1_cell_1e.csv";	
				w_c4 = new FileWriter(weightsFileName);
				mlog.say("stream opened "+weightsFileName);
				
				weightsFileName = folderName+"/L1_ws0_0e.csv";	
				w_c5 = new FileWriter(weightsFileName);
				mlog.say("stream opened "+weightsFileName);
				
				weightsFileName = folderName+"/L1_cell_0e.csv";	
				w_c6 = new FileWriter(weightsFileName);
				mlog.say("stream opened "+weightsFileName);
				
				weightsFileName = folderName+"/L1_ws1_0e.csv";	
				w_c7 = new FileWriter(weightsFileName);
				mlog.say("stream opened "+weightsFileName);
				
				
				weightsFileName = folderName+"/L1_ws2_0e.csv";	
				w_c8 = new FileWriter(weightsFileName);
				mlog.say("stream opened "+weightsFileName);
				
				weightsFileName = folderName+"/L1_ws3_0e.csv";	
				w_c9 = new FileWriter(weightsFileName);
				mlog.say("stream opened "+weightsFileName);
				
				weightsFileName = folderName+"/L1_ws4_0e.csv";	
				w_c10 = new FileWriter(weightsFileName);
				mlog.say("stream opened "+weightsFileName);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		//main loop
		while(epoch<=Constants.L1_cycle*6){
			//write cell data
			if(Constants.save){
				try {
					String str = colonies[0].cells[32].stateToString();
		        	w_c1.append(str);
		        	w_c1.flush();
		        	
		        	str = colonies[1].cells[0].stateToString();
		        	w_c2.append(str);
		        	w_c2.flush();
		        	
		        	str = colonies[0].cells[33].stateToString();
		        	w_c3.append(str);
		        	w_c3.flush();
		        	
		        	str = colonies[1].cells[1].stateToString();
		        	w_c4.append(str);
		        	w_c4.flush();
		        	
		        	str = colonies[0].wsToString(0);
		        	w_c5.append(str);
		        	w_c5.flush();
		        	
		        	str = colonies[0].leveStateToString();
		        	w_c6.append(str);
		        	w_c6.flush();
		        	
		        	str = colonies[0].wsToString(1);
		        	w_c7.append(str);
		        	w_c7.flush();
		        	
		        	str = colonies[0].wsToString(2);
		        	w_c8.append(str);
		        	w_c8.flush();
		        	
		        	str = colonies[0].wsToString(3);
		        	w_c9.append(str);
		        	w_c9.flush();
		        	
		        	str = colonies[0].wsToString(4);
		        	w_c10.append(str);
		        	w_c10.flush();
		        	
		        	/*int a = colonies[0].getAddress();
					mlog.say("colony 0 address is "+ a);//*/
				} catch (IOException e) {
					e.printStackTrace();
				}
			}		
			
			//apply transfer rules to everyone
			for(int i=0; i<m; i++){
				Colony c = colonies[i];
				c.compute(Constants.e);
			}
			
			//copy computed state to current sate
			for(int i=0; i<m; i++){
				Colony c = colonies[i];
				c.update();
			}
			
			//slow the program down
			try {
				Thread.sleep(000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			epoch++;
			//mlog.say("epoch "+epoch);
		}
	}
	
	
	@Override
	public void draw(Graphics g) {	
		Graphics2D g2d = (Graphics2D) g;

		//cleanup
		g2d.clearRect(0,0,1000,1000);
		
		//draw each colony
		int n = Math.min(5,m);
		int nc =  Math.min(Constants.Q,Constants.nc_draw);
		for(int i=0; i<n; i++){
			//draw colony
			colonies[i].draw(g2d, i, nc);
		}	
	}
	
}
