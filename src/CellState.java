import java.util.BitSet;

/**
 * Holds the fields of a cell.
 * @author lana
 *
 */
public class CellState {
	//logging has been outsourced.
		
	/**the whole state. A pointer implementation could be more beautiful, but this is java.*/
	BitSet state;
	/** the size of each state field*/
	int[] field_sizes = Constants.field_sizes;
	/** fields indices in the bitstring*/
	int[] fields_bit_i = Constants.fields_bit_i();
	//for performance issues 
	protected int age_int = 0;
	protected int address_int;
	
	/**
	 * Unspecified sites are initialized to 0.
	 * @param address the address of this cell in its colony.
	 */
	public CellState(int address){
		//alloc
		state = new BitSet(Constants.state_nbits);
		//convert address integer to bit array
		BitSet a = Constants.intToBitset(address);
		//init
		this.setField(Constants.address_i, a);	
		address_int = address;
	}
	
	/***
	 * returns a random cell state or a blank state
	 * @param random true if random state
	 */
	public CellState(boolean random){
		if(random){
			state = randomSet(Constants.state_nbits);
			address_int = this.getAddressIntFromBits();
		} else {
			state = new BitSet(Constants.state_nbits);
		}
	}

	
	/**
	 * 
	 * @param address
	 * @param b
	 */
	public CellState(int address, boolean b) {
		//alloc
		state = new BitSet(Constants.state_nbits);
		
		//convert address integer to bit array
		BitSet a = Constants.intToBitset(address);
		//init address
		this.setField(Constants.address_i, a);	
		address_int = address;
		
		//init sim
		this.setFlag(Constants.simBit_i, 0, b);
		/*if(b){
			MyLog mLog = new MyLog("cstate", true);
			mLog.say("set "+ address);
		}*/
	}

	/**
	 * TODO: move to Utils or Constants file.
	 * @param size the size of the set to be returned.
	 * @return a set of bits where some bits were randomly chosen to be 1
	 * according to the probability distribution s.
	 */
	private BitSet randomSet(int size){
		BitSet b = new BitSet(size);
		
		for(int i=0; i<size; i++){
			if(Constants.uniformDouble()>Constants.s_rate){
				b.set(i);
			}
		}
		
		return b;
	}
	
	
	/**
	 * @return an int corresponding to the value of the bit array in te address field
	 */
	public int getAddressInt(){
		return address_int;
	}
	
	
	/**
	 * recalculate address from bits. Costs more CPU than simple getter.
	 * @return
	 */
	public int getAddressIntFromBits(){
		return Constants.bitsetToInt(this.getField(Constants.address_i));
	}
	
	/**
	 * purely for test purposes
	 * @param a int representing this cell's address 
	 */
	public void setAddressInt(int a){		
		BitSet ad = Constants.intToBitset(a);
		this.setField(Constants.address_i, ad);
		address_int = a;
	}
	
	/**
	 * @return an int corresponding to the bit array in this field
	 */
	public int getAgeInt(){
		return age_int;
	}
	
	/**
	 * @return the value of this bit (true = 1)
	 */
	public boolean primarySimbit(){
		return this.flag(Constants.simBit_i,0);//TODO why is this a freakin flag??? not in paper??
	}
	
	/**
	 * @return true if this flag is raised
	 */
	public boolean flag1(){	
		return this.flag(Constants.flags_i, 0);
	}
	
	
	/**
	 * adds +1 to the age field
	 */
	public void ageThisCell(){
		int a = Constants.bitsetToInt(this.getField(Constants.age_i));
		a++;
		//if age == U then age = 0 ( 0>age>(u-1) )
		if(a>=Constants.U){
			a = 0;
		}	
		age_int = a;
		this.setField(Constants.age_i, Constants.intToBitset(a));
	}
	
	
	/**
	 * @param b true to raise this flag, false to lower it.
	 */
	public void setFlag1(boolean b){
		this.setFlag(Constants.flags_i,0, b);
	}
	
	/**
	 * @param b true to raise this flag, false to lower it.
	 */
	public void setWorkspaceFlag1(boolean b){
		this.setFlag(Constants.workspace_i,0, b);
	}
	
	/**
	 * @param b true to raise this flag, false to lower it.
	 */
	public void setWorkspaceFlag2(boolean b){
		this.setFlag(Constants.workspace_i,1, b);
	}
	
	/**
	 * @param b true to raise this flag, false to lower it.
	 */
	public void setFlag2(boolean b){
		this.setFlag(Constants.flags_i,1, b);
	}
	
	
	/**
	 * @return an exact copy of this state.
	 */
	public CellState copy(){
		CellState s = new CellState(this.getAddressInt());
		s.age_int = this.age_int;
		s.state = (BitSet) this.state.clone();
		return s;
	}

	/**
	 * @return true if this flag is raised
	 */
	public boolean flag2() {
		return this.flag(Constants.flags_i, 1);
	}

	/**
	 * set the age field of this cell to the parameter
	 * @param age age to be set to.
	 */
	public void setAgeInt(int age) {
		this.setField(Constants.age_i, Constants.intToBitset(age));
		age_int = age;
	}

	/**
	 * @return true if flag==1, false if flag==0
	 */
	public boolean workspaceFlag1() {
		return this.flag(Constants.workspace_i,0);
	}
	
	/**
	 * @return true if flag==1, false if flag==0
	 */
	public boolean workspaceFlag2() {
		return this.flag(Constants.workspace_i,1);
	}

	/**
	 * 
	 * @return the address field of this state
	 */
	public BitSet getAddressBits() {
		return getField(Constants.address_i);
	}
	
	/** set both mail info and sender*/
	public void setMail(BitSet mail, int copy) {
		int size = Constants.mailbox_from_nbits + Constants.mailbox_info_nbits;
		int base_index = size*copy;
		this.setSubField(Constants.mailbox_i, base_index, size, mail);
	}
	
	/**
	 * sets the FROM subfield of the mailbox field
	 * @param sender
	 */
	public void setMailFrom(BitSet sender, int copy) {
		int size = Constants.mailbox_from_nbits + Constants.mailbox_info_nbits;
		int base_index = size*copy;
		this.setSubField(Constants.mailbox_i, base_index+Constants.mailbox_from_s, Constants.mailbox_from_nbits, sender);
	}
	
	/**
	 * @param int copy: index of the set: 0->this cell; 1,2->backup for cell -1,-2; 3,4->backup for cells 1,2
	 * @return the FROM subfield of the mailbox field
	 */
	public BitSet getMailFrom(int copy) {
		int base_index = (Constants.mailbox_from_nbits + Constants.mailbox_info_nbits)*copy;
		return getSubField(Constants.mailbox_i, base_index+Constants.mailbox_from_s, Constants.mailbox_from_nbits);
	}
	
	/**
	 * sets the INFO subfield of the mailbox field
	 * @param sender
	 */
	public void setMailInfo(BitSet info, int copy) {
		int size = Constants.mailbox_from_nbits + Constants.mailbox_info_nbits;
		int base_index = size*copy;
		this.setSubField(Constants.mailbox_i, base_index+Constants.mailbox_info_s, Constants.mailbox_info_nbits, info);
	}
	
	/**
	 * @param int copy: index of the set: 0->this cell; 1,2->backup for cell -1,-2; 3,4->backup for cells 1,2
	 * @return the INFO subfield of the mailbox field
	 */
	public BitSet getMailInfo(int copy) {
		int base_index = (Constants.mailbox_from_nbits + Constants.mailbox_info_nbits)*copy;
		return getSubField(Constants.mailbox_i, base_index+Constants.mailbox_info_s, Constants.mailbox_info_nbits);
	}
	
	
	//utility functions
	
	/**
	 * sets a state field to some value
	 * @param field_i index of the field to be set relative to other fields
	 * @param source the value to set the field to
	 */
	private void setField(int field_i, BitSet source){
		//source_length the number of bits in source -- needed to bypass useless java 
		// methods like BitSet.size/length.
		int source_length = field_sizes[field_i];
		//position of field in state bitSet
		int start = fields_bit_i[field_i];
		state.clear(start, start+source_length);
		int j=0;
		for(int i=start;i<start+source_length;i++){
			if(source.get(j)) state.set(i);
			j++;
		}
	}
	
	/**
	 * sets a state subfield to some value
	 * @param field_i index of the field to be set relative to other fields
	 * @param subfield_s index of 1st bit of the subfield relative to field
	 * @param s size of subfield
	 * @param source the value to set the field to
	 */
	public void setSubField(int field_i, int subfield_s, int s, BitSet source){
		int start = fields_bit_i[field_i] + subfield_s;
		state.clear(start, start+s);
		int j=0;
		for(int i=start;i<start+s;i++){
			if(source.get(j)){
				state.set(i);
			}
			j++;
		}
	}
	
	/**
	 * @param flag_field index, inside which field is this flag situated 
	 * @param flag_index its index in the field
	 * @return
	 */
	public boolean flag(int flag_field, int flag_index){
		//position in state bitSet
		int start = fields_bit_i[flag_field]+flag_index; 
		return state.get(start,start+1).get(0);//flags are always 1bit
	}
	
	/**
	 * 
	 * @param field_i
	 * @return
	 */
	public BitSet getField(int field_i){
		//position in state bitSet
		int start = fields_bit_i[field_i];
		return state.get(start,start+field_sizes[field_i]);
	}
	
	/**
	 * 
	 * @param field_i
	 * @param subfield_s index of 1st bit of the subfield relative to field
	 * @param s size of subfield
	 * @return
	 */
	public BitSet getSubField(int field_i, int subfield_s, int s){
		//position in state bitSet
		int start = fields_bit_i[field_i] + subfield_s;
		return state.get(start,start+s);
	}
	
	/**
	 * @param flag_field index, inside which field is this flag situated 
	 * @param flag_index its index in the field
	 * @param raise
	 */
	public void setFlag(int flag_field, int flag_index, boolean raise){
		int start = fields_bit_i[flag_field]+flag_index;
		if(raise){
			state.set(start);
		}else{
			state.clear(start);
		}
	}


	/**
	 * sets all mailbox bits to 0: sender and info
	 */
	public void clearMailbox(int copy) {
		int size = Constants.mailbox_from_nbits + Constants.mailbox_info_nbits;
		int base_index = size*copy;
		int start = fields_bit_i[Constants.mailbox_i] + base_index;
		state.clear(start, start+size);
	}

	/**
	 * sets simbits, workspace bits to 0
	 * incuding backups stored in this cell
	 * but not backups of this cell
	 */
	public void clearSimfields() {
		//clear simbit and stored backups
		int start = fields_bit_i[Constants.simBit_i];
		state.clear(start, start+Constants.simbit_nbits);
		
		//clear ws and backups
		start = fields_bit_i[Constants.workspace_i];
		state.clear(start, start+Constants.workspace_nbits);
	}

	public BitSet getState() {
		return state;
	}

	public void setState(BitSet state) {
		this.state = state;
	}

	/**
	 * 
	 * @param index bit index
	 * @param value
	 */
	public void setBit(int index, boolean value) {
		if(value){
			state.set(index);
		}else{
			state.clear(index);
		}
	}

	public void setSimBit(boolean b) {
		setFlag(Constants.simBit_i, 0, b);
	}

	public void clearWorkspaceInfo(int copy) {
		int size = Constants.workspace_info_nbits;
		int base_index = size*copy + Constants.workspace_info_s;
		int start = fields_bit_i[Constants.workspace_i] + base_index;
		state.clear(start, start+size);
	}

	/**
	 * get sender and info
	 * @param copy which copy in this field
	 * @return
	 */
	public BitSet getWholeMail(int copy) {
		int base_index = (Constants.mailbox_from_nbits + Constants.mailbox_info_nbits)*copy;
		return getSubField(Constants.mailbox_i, base_index, Constants.mailbox_nbits/5);
	}

	public void setWoleMail(BitSet mail, int copy) {
		int size = Constants.mailbox_from_nbits + Constants.mailbox_info_nbits;
		int base_index = size*copy;
		this.setSubField(Constants.mailbox_i, base_index, size, mail);
	}

	public void setWSi(int to_wsi, boolean b) {
		int index = Constants.workspace_info_s+to_wsi;
		this.setFlag(Constants.workspace_i, index, b);
	}
	
}
