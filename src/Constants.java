import java.awt.Color;
import java.util.BitSet;
import java.util.Random;

/**
 * initialize model's values here.
 * We might sometimes use long int instead of int values, because for the value of Q suggested by gray (2^23)
 * the number of bits necessary to store "the number of bits inside in the workspace" 53 -> max int is 32 bits..
 * @author lana
 *
 */
public class Constants {
	static MyLog mlog = new MyLog("Constants", true);
	/** use G or not*/
	static boolean autonomous = true;
	
	/** save data or not*/
	static boolean save = true;
	
	/** helps calling building functions only once*/
	static boolean built = false;
	
	/** duration of 1 L1 cycle for L0 cells */
	public static int L1_cycle = 24027;//20419;
	
	/** This value is unknown but important to define max noise e in Gacs. Maybe not in Gray.*/
	//R0
	/** noise parameter: the probability that any given cell will be damaged by an "error"*/
	public static double e = 0.001;//Math.pow(Q*U, -2);// 0.0; //QU^-2
	/** noise variables: the distribution is irrelevant. I decided uniform (0,1) distribution on all bits.
	 * The "error" is a bit string, not a single bit.*/
	public static double s_rate = 0.5;
	
	/**display graphics or not*/
	public static final boolean draw = false;	
	/**how many cells of the colony to draw*/
	public static final int nc_draw = 30;
	/** controls output of all loggers. Set true for verbose mode, false for dry mode.*/
	public static final boolean shouldLog = true;

	//field sizes
	/** address field size in bits (in Gray's model, it's an integer)*/
	public static final int a_nbits = Integer.SIZE;//max value = log2(128*Q) = 7+?.
	/** size of the "Flags" field, in bits (Flag 1 + Flag 2)*/
	public static final int flags_nbits = 2;
	/** size of the SimBit field: 1+backup (p33)*/
	public static final int simbit_nbits = 5;//order: i,i-2,i-1,i+1,i+2
	
	//mailbox subfields:
	/** stores the sender's address*/
	public static final int mailbox_from_nbits = a_nbits;
	/** position of FROM subfield*/
	public static final int mailbox_from_s = 0;
	/** stores mail content: max size = 1 bit */
	public static final int mailbox_info_nbits = 1;//p29
	/** position of INFO subfield*/
	public static final int mailbox_info_s = mailbox_from_nbits;
	/** number of bits in whole mailbox field*/
	public static final int mailbox_nbits = (mailbox_from_nbits + mailbox_info_nbits)*5;

	//workspace subfields
	//missing: workspace "signal", workspace backup
	//TODO flag1, flag2
	/** this info field is after the flags*/
	public static final int workspace_info_s = 2;
	/** size of ws : 1 bit from self + 2*5 neighbor colonies */
	public static final int workspace_info_nbits = 11;
	/** number of bits in the workspace. */
	public static final int workspace_nbits = workspace_info_s+(workspace_info_nbits*5);//(int)(const_a*qu_bits);//upper bound
	
	/** total size of a state, in bits: address, age, flags, sim, ws, mb*/
	public static final int state_nbits = a_nbits*2+flags_nbits+simbit_nbits+workspace_nbits+mailbox_nbits;

	/** size of a colony: number of cells. a power of 2. Q>=2state_nbits*/
	public static final int Q = (int) state_nbits;//Math.pow(2, 8);//real min value = 2^23, 8,388,608 
	/** How many colonies are in the model: model size*/
	public static final int M = 10;//Q*1;
	/**work period of the model*/
	public static final int U = L1_cycle;//128*Q;
	
	/** the interaction range of one cell (5 in Gray's model)*/
	public static final int range = 5;
	
	
	/**page 15 of Gray's. Determines size of the model? unknown value.*/
	public static final int const_a = 1;
	//this calculation is necessary to avoid int overflow in next lines
	static int qu_bits = (int) ((Math.log(Q)/Math.log(2)) + (Math.log(U)/Math.log(2)));

	//moved here to reduce memory usage in cell instances
	/** the size of each state field: address, age, flags, sim, ws, mb*/
	public static final int[] field_sizes = {Constants.a_nbits, Constants.a_nbits, Constants.flags_nbits, Constants.simbit_nbits,
						 Constants.workspace_nbits, Constants.mailbox_nbits};
	/** the bit index of each field*/
	private static final int[] fields_bit_i = new int[field_sizes.length];
	/** index of cell address in the bitstring*/
	public static final int address_i = 0;
	/** int , {@literal 0<age<U-1}*/
	public static final int age_i = address_i+1;
	/**index of Flag1 and Flag2 */
	public static final int flags_i = age_i +1;
	/** simbits */
	public static final int simBit_i = flags_i+1;
	/**index of workspace*/
	public static final int workspace_i = simBit_i+1;
	/**index of mailbox; for communication with other cells*/
	public static final int mailbox_i = workspace_i+1;

	//self simulation variables
	/** start of super-cell state in colony*/
	public static int super_c_index = 0;//TODO use in G class
	
	//color coding. for more precise info on consistency of cell self-knowledge
	//with actual expected value, see csv data files.
	//external knowledge
	/** Graphical parameter. Cell color if it is in the error set (even if not really damaged)*/
	public static final Color c_damaged = Color.red;
	/** Graphical parameter. Color of the cell if it is not in the error set at this time-step*/
	public static final Color c_normal = Color.black;
	//self-knowledge
	/** Graphical parameter. Cell color if no flags raised */
	public static final Color c_healthy = Color.blue;
	/** Graphical parameter. Cell color if flags raised - waiting for correction*/
	public static final Color c_flagged = Color.green;

	//field ids
	public static final int MAILFIELD = 1;
	
	//values that must be calculated live
	public static int[] fields_bit_i(){
		if(!built){
			String field_names[] = {"address", "age", "flags", "sim", "ws", "mb"};
			//mlog.say("address, age, flags, sim, ws, mb");
			for(int i=0;i<field_sizes.length;i++){
				fields_bit_i[i] = sum(field_sizes, i);
				mlog.say(""+ field_names[i] + " index " + fields_bit_i[i]);
			}
			built = true;
		}
		return fields_bit_i;
	}
	
	
	//some useful functions
	/**
	 * convert int to BitSet (little-endian0
	 * @param anint the int to be converted
	 * @return the corresponding bitset
	 */
	public static BitSet intToBitset(int anint){
		BitSet bitset;
		bitset = BitSet.valueOf(new long[]{anint});
		return bitset;
	}
	
	
	/**
	 * Convert bits (little-endian) to integer
	 * 
	 * @param bitset the BitSet to be converted
	 * @return the corresponding int
	 */
	public static int bitsetToInt(BitSet bitset){
		int r = 0;
		int pow = 1;
		for(int i=0; i<Integer.SIZE;i++,pow<<=1){
			if(bitset.get(i))
				r|=pow;
		}
		return r;
	}
	
	/**
	 * Uniform distribution between [0,1]
	 * @return random number
	 */
	public static double uniformDouble() {
		double min = 0;
		double max = 1;
	    // NOTE: Usually this should be a field rather than a method
	    // variable so that it is not re-seeded every call.
	    Random rand = new Random();

	    // nextInt is normally exclusive of the top value,
	    // so add 1 to make it inclusive
	    double randomNum = min + (max - min) * rand.nextDouble();
	    //rand.nextInt(max - min) + 1)

	    return randomNum;
	}
	
	/**
	 * performs a sum on an array.
	 * @param array the array of which values to be summed
	 * @param to_index the sum will be from 0 to (to_index-1) inclusive
	 * @return the calculated sum
	 */
	public static int sum(int[] array, int to_index){
		int s = 0;
		for(int i=0; i<to_index; i++){
			s+=array[i];
		}
		return s;
	}
	
}
